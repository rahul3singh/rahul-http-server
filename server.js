const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require('uuid');
http
  .createServer((req, res) => {
    const url = req.url;
    const urlArray = url.split("/");
    const statusCode = Number(urlArray[urlArray.length - 1]);
    const delay = statusCode;
    switch (req.url) {
      case "/html":
        fs.readFile("./index.html", "UTF-8", (err, data) => {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.write(data);
          res.end();
        });
        break;
       case "/json":
        fs.readFile("./data.json", "UTF-8", (err, data) => {
          res.writeHead(200, { "Content-Type": "text/json" });
          res.write(data);
          res.end();
        });
        break;
       case "/uuid":
        const uid = JSON.stringify({uuid : uuidv4()});
        res.writeHead(200, {"Content-Type": "text/json"})
        res.write(uid);
        res.end()
        break;
        case `/status/${statusCode}`:
        res.writeHead(statusCode, { "Content-Type": "text/plain" });
        switch (statusCode) {
          case 100:
            res.write("Informational");
            res.end();
            break;
          case 200:
            res.write("Success");
            res.end();
            break;

          case 300:
            res.write("Redirection");
            res.end();
            break;

          case 400:
            res.write("Client Error");
            res.end();
            break;

          case 500:
            res.write("Server Error");
            res.end();
            break;
        }
        break;
        case `/delay/${delay}`:
        setTimeout(() => {
          res.writeHead(200, { "Content-Type": "text/plain" });
          res.write(`Result got delayed ${delay} second`);
          res.end();
        }, delay * 1000);
        break;
        default:
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.write("This is Home Page");
        res.end();
        break;
       }
})
  .listen(4000);